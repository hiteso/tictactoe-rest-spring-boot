package com.al.TicTacToe.controllers;

import com.al.TicTacToe.models.Game;
import com.al.TicTacToe.models.GameActionsRequestWrapper;
import com.al.TicTacToe.models.GameManager;
import com.al.TicTacToe.models.MemoryGameStore;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/")
public class MainController {

    @PostMapping("/games")
    public ResponseEntity<Game> postGames(HttpSession session) {

        GameManager gameManager = new GameManager(session.getId(), new MemoryGameStore());
        Game game = gameManager.newGame();
        return new ResponseEntity<>(game, HttpStatus.CREATED);
    }

    @GetMapping("/games/{gameId}")
    public ResponseEntity<Game> getGames(HttpSession session, @PathVariable Long gameId) {

        GameManager gameManager = new GameManager(session.getId(), new MemoryGameStore());
        Game game = gameManager.getGame(gameId);
        if (null != game) {
            return new ResponseEntity<>(game, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/games/{gameId}/players")
    public ResponseEntity<Object> postPlayers(HttpSession session, @PathVariable Long gameId) {

        GameManager gameManager = new GameManager(session.getId(), new MemoryGameStore());
        if (!gameManager.addPlayerToGame(gameId)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping("/games/{gameId}/actions")
    public ResponseEntity<Object> postActions(HttpSession session, @PathVariable Long gameId, @RequestBody GameActionsRequestWrapper requestWrapper) {

        GameManager gameManager = new GameManager(session.getId(), new MemoryGameStore());
        if (!gameManager.performActionInGame(gameId, requestWrapper.x, requestWrapper.y)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}