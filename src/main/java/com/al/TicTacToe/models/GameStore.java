package com.al.TicTacToe.models;

public interface GameStore {

    Game put(Game game) throws RuntimeException;

    Game get(Long id);
}
