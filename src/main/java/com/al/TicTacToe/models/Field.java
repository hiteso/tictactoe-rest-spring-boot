package com.al.TicTacToe.models;

public class Field {

    private final int size;
    private final char[] field;

    private int count;

    public Field(int size) {

        if (size > Math.sqrt(Integer.MAX_VALUE) - 8 || size < 2) {
            throw new IllegalArgumentException();
        }
        this.size = size;
        field = new char[size * size];
    }

    public char getWinner() {

        if (isWinner('x')) {
            return 'x';
        }
        if (isWinner('o')) {
            return 'o';
        }
        return 0;
    }

    private boolean isWinner(char c) {

        boolean d1 = true, d2 = true, v = true, g = true;
        for (int y = 0; y < size; y++) {
            d1 &= field[indexOf(y, y)] == c;
            d2 &= field[indexOf(size - 1 - y, y)] == c;
            for (int x = 0; x < size; x++) {
                g &= field[indexOf(x, y)] == c;
                v &= field[indexOf(y, x)] == c;
            }
        }
        return d1 || d2 || g || v;
    }

    public boolean isFull() {
        return count == size * size;
    }

    public boolean set(char c, int x, int y) {

        boolean inBounds = y >= 0 && y < size;
        inBounds = inBounds && x >= 0 && x < size;
        if (inBounds) {
            int index = indexOf(x, y);
            if (0 == field[index]) {
                field[index] = c;
                count++;
                return true;
            }
        }
        return false;
    }

    public char[][] toSquareField() {

        char[][] square = new char[size][size];
        char c;
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                c = field[indexOf(x, y)];
                square[x][y] = 0 == c ? '?' : c;
            }
        }
        return square;
    }

    private int indexOf(int x, int y) {
        return x + y * size;
    }
}
