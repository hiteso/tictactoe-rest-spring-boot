package com.al.TicTacToe.models;

import java.util.HashMap;
import java.util.Map;

public class MemoryGameStore implements GameStore {

    static private final Map<Long, Game> store = new HashMap<>();

    @Override
    synchronized public Game put(Game game) {
        Game storedGame = store.put(game.getId(), game);
        if (null != storedGame) {
            store.put(storedGame.getId(), storedGame);
            throw new RuntimeException("Primary key collision");
        }
        return null;
    }

    @Override
    public Game get(Long id) {
        return store.get(id);
    }
}
