package com.al.TicTacToe.models;

public class GameManager {

    private final String playerId;
    private final GameStore store;

    public GameManager(String playerId, GameStore store) {

        this.playerId = playerId;
        this.store = store;
    }

    public Game newGame() {

        Game game = new Game();
        if (!game.addPlayer(playerId)) {
            throw new RuntimeException();
        }
        store.put(game);
        return game;
    }

    public boolean addPlayerToGame(long gameId) {

        Game game = store.get(gameId);
        return null != game && game.addPlayer(playerId);
    }

    public boolean performActionInGame(long gameId, int x, int y) {

        Game game = store.get(gameId);
        return null != game && game.isPlayerInGame(playerId) && game.performAction(playerId, x, y);
    }

    public Game getGame(Long gameId) {

        Game game = store.get(gameId);
        return null != game && game.isPlayerInGame(playerId) ? game : null;
    }
}
