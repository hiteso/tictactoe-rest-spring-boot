package com.al.TicTacToe.models;

public class Game {

    static private long nextId = System.currentTimeMillis();

    private final long id = nextId();

    private final Field field;

    private String xPlayerId;
    private String oPlayerId;
    private String nextActionPlayerId;
    private State state = State.WAITING_MORE_PLAYERS;

    public Game() {
        field = new Field(3);
    }

    public Game(Field field) {
        this.field = field;
    }

    synchronized static private long nextId() {
        return nextId++;
    }

    public long getId() {
        return id;
    }

    public char[][] getField() {
        return field.toSquareField();
    }

    public String getState() {
        return state.toString();

    }

    public boolean isPlayerInGame(String playerId) {

        return playerId.equals(xPlayerId) || playerId.equals(oPlayerId);
    }

    synchronized public boolean addPlayer(String playerId) {

        if (null == xPlayerId) {
            nextActionPlayerId = xPlayerId = playerId;
            return true;
        }
        if (playerId.equals(xPlayerId)) {
            return false;
        }
        if (null == oPlayerId) {
            oPlayerId = playerId;
            state = State.WAITING_X_PLAYER;
            return true;
        }
        return false;
    }

    synchronized public boolean performAction(String playerId, int x, int y) {
        return isPlayerNextMove(playerId) && performAction(x, y);
    }

    private boolean isPlayerNextMove(String playerId) {

        switch (state) {
            case X_PLAYER_WIN:
            case O_PLAYER_WIN:
            case NOBODY_WIN:
                return false;
        }
        return playerId.equals(nextActionPlayerId);
    }

    private boolean performAction(int x, int y) {

        char c = nextActionPlayerId == xPlayerId ? 'x' : 'o';
        if (field.set(c, x, y)) {
            nextActionPlayerId = nextActionPlayerId == xPlayerId ? oPlayerId : xPlayerId;
            updateState();
            return true;
        }
        return false;
    }

    private void updateState() {

        switch (field.getWinner()) {
            case 'x':
                state = State.X_PLAYER_WIN;
                break;
            case 'o':
                state = State.O_PLAYER_WIN;
                break;
            default:
                if (field.isFull()) {
                    state = State.NOBODY_WIN;
                } else {
                    state = nextActionPlayerId == xPlayerId ? State.WAITING_X_PLAYER : State.WAITING_O_PLAYER;
                }
                break;
        }
    }

    public enum State {WAITING_MORE_PLAYERS, WAITING_X_PLAYER, WAITING_O_PLAYER, X_PLAYER_WIN, O_PLAYER_WIN, NOBODY_WIN}

    ;
}
